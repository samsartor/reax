use reax::prelude::*;
use std::rc::Rc;

type List = Var<Vec<Rc<Var<i32>>>>;

fn main() {
    let list: List = Var::new(Vec::new()).with_label("list");
    let sum = computed! {
        list.get().iter().map(|x| x.get_copy()).sum::<i32>()
    }
    .with_label("sum");

    let mut watchers = EagerCompute::new(());
    watchers.dbg(&sum);
    watchers.dbg(&list);
    watchers.tick();

    for name in &["a", "b", "c"] {
        list.mutate().push(Rc::new(Var::new(1).with_label(name)));
        watchers.tick();
    }

    let x = Rc::new(Var::new(0).with_label("x"));
    watchers.dbg(x.clone());
    list.mutate().push(x.clone());
    watchers.tick();

    for i in 1..=5 {
        x.set(i);
        watchers.tick();
    }

    for r in list.get().iter() {
        *r.mutate() -= 1;
    }

    reax::ThreadRuntime::write_graphviz(std::io::stdout().lock()).unwrap();
    watchers.tick()
}
