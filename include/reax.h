#pragma once

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef size_t ReaxNode;

typedef void* ReaxDirtiedHandler;

/**
 * Create a new node in the reax runtime's dependency graph. ReaxNodes are the
 * basic primitive to implement variables. This node is unique across theads but
 * the runtime itself is thread local.
 */
ReaxNode reax_node_create();

void reax_node_destroy(ReaxNode to_destroy);

bool reax_node_is_dirty(ReaxNode is_dirty);

void reax_node_on_read(ReaxNode will_read);

void reax_node_on_write(ReaxNode will_write, bool keep_dirty);

size_t reax_node_num_dependencies(ReaxNode dependencies_of);

size_t reax_node_num_dependents(ReaxNode dependents_of);

void reax_node_add_dependency(ReaxNode dependent, ReaxNode dependency);

void reax_node_send_dirtied_signal_to(
  ReaxNode signals_on,
  ReaxDirtiedHandler send_to);

void reax_computation_push(ReaxNode to_compute);

void reax_computation_pop();

bool reax_computation_is_reactive();

void reax_computation_set_reactive(bool reactive);

ReaxDirtiedHandler reax_dirtied_handler_create(
  void waker(void*),
  void finalizer(void*),
  void* data);

void reax_dirtied_handler_reference(ReaxDirtiedHandler handler);

void reax_dirtied_handler_release(ReaxDirtiedHandler handler);

size_t reax_dirtied_handler_poll(
  ReaxDirtiedHandler handler,
  size_t* id_buffer,
  size_t capacity);

size_t reax_write_graphviz(unsigned char* buffer, size_t capacity);

#ifdef __cplusplus
}
#endif
