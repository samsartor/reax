#pragma once

#include "reax.h"
#include <functional>
#include <unordered_map>
#include <utility>

namespace reax {
  template<typename T, typename F>
  class ComputedVar;

  template<typename T>
  class Variable {
  public:
    ReaxNode node;

    Variable(): node(reax_node_create()) { }
    ~Variable() { reax_node_destroy(node); }

    virtual const T& getNonReactive() = 0;

    const T& get() {
      reax_node_on_read(node);
      return getNonReactive();
    }

    operator const T&() {
      return get();
    }
  };

  template<typename T>
  class Var: public Variable<T> {
  public:
    Var() { }
    Var(T value): value_(value) { }
    Var(Var&& var) =default;

    Var(const Var&) =delete;
    Var& operator=(const Var&) =delete;

    T& operator =(const T& value) {
      mutate() = value;
      return value_;
    }

    const T& getNonReactive() override {
      return value_;
    }

    T& mutate() {
      reax_node_on_write(Variable<T>::node, true);
      return value_;
    }


  protected:
    T value_;
  };

  class ComputationHandle {
  public:
    ComputationHandle(ReaxNode node) {
      reax_computation_push(node);
    }

    ~ComputationHandle() {
      reax_computation_pop();
    }

    ComputationHandle(const ComputationHandle&) =delete;
    ComputationHandle& operator=(const ComputationHandle&) =delete;
  };

  template<typename T, typename F>
  class ComputedVar: public Variable<T> {
  public:
    ComputedVar(F computer): computer_(computer) { }
    ComputedVar(ComputedVar&& var) =default;

    ComputedVar(const ComputedVar&) =delete;
    ComputedVar& operator=(const ComputedVar&) =delete;

    const T& getNonReactive() override {
      if (reax_node_is_dirty(Variable<T>::node)) {
        ComputationHandle computation { Variable<T>::node };
        reax_node_on_write(Variable<T>::node, false);
        value_ = computer_();
      }
      return value_;
    }

  protected:
    T value_;
    F computer_;
  };

  template<typename F>
  auto computed(F function) -> ComputedVar<decltype(function()), F> {
    return ComputedVar<decltype(function()), F>(function);
  }

  class EagerCompute {
  public:
    EagerCompute():EagerCompute([]() {}) { }

    EagerCompute(ReaxDirtiedHandler handler):handler_(handler) {
      reax_dirtied_handler_reference(handler_);
    }

    EagerCompute(std::function<void()>&& waker):
      handler_(reax_dirtied_handler_create(
        [](void* f) { (*(std::function<void()>*) f)(); },
        [](void* f) { delete (std::function<void()>*) f; },
        (void*) new std::function<void()>(std::move(waker)))) { }

    ~EagerCompute() {
      reax_dirtied_handler_release(handler_);
      for (auto& entry : vars_) {
        reax_node_destroy(entry.first);
      }
    }

    EagerCompute(const EagerCompute&) =delete;
    EagerCompute& operator=(const EagerCompute&) =delete;

    void watch(std::function<void()>&& watcher) {
      ReaxNode node = reax_node_create();
      {
        ComputationHandle computation { node };
        reax_node_on_write(node, false);
        watcher();
      }
      reax_node_send_dirtied_signal_to(node, handler_);
      vars_.emplace(node, std::forward<std::function<void()>>(watcher));
    }

    size_t poll() {
      ReaxNode nodes[32];
      size_t count = reax_dirtied_handler_poll(handler_, nodes, 32);
      for (int i = 0; i < count; i++) {
        auto entry = vars_.find(nodes[i]);
        if (entry != vars_.end()) {
          ComputationHandle(entry->first);
          reax_node_on_write(entry->first, false);
          entry->second();
        }
      }
      return count;
    }

  protected:
    ReaxDirtiedHandler handler_;
    std::unordered_map<ReaxNode, std::function<void()>> vars_;
  };
}
