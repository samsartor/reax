use reax::prelude::*;
use reax::Node;
use std::fs::File;
use std::rc::Rc;

#[test]
fn lazy_computed_sum() {
    let a = Var::new(2);
    let b = Var::new(2);
    let times = Var::new(0);
    let sum = computed! {
        *times.mutate() += 1;
        a.get_copy() + b.get_copy()
    };

    // The sum is lazily computed.
    assert_eq!(sum.node().is_dirty(), true);
    assert_eq!(sum.node().num_dependencies(), 0);

    sum.check(&mut ());
    assert_eq!(sum.node().num_dependencies(), 2);
    assert_eq!(times.get_copy(), 1);
    assert_eq!(sum.get_copy(), 4);
    assert_eq!(times.get_copy(), 1);

    a.set(3);

    assert_eq!(times.get_copy(), 1);
    assert_eq!(sum.get_copy(), 5);
    assert_eq!(times.get_copy(), 2);
}

#[test]
fn lazy_computed_format() {
    use std::fmt::Write;

    let name = Var::new("world".to_owned());
    let text = computed! {
        output! text = String::new();
        text.clear();
        write!(text, "hello {}", name.get()).unwrap();
    };

    assert_eq!(text.get().as_str(), "hello world");
    name.mutate().replace_range(.., "sam");
    assert_eq!(text.get().as_str(), "hello sam");
}

#[test]
fn eager_computed_sum() {
    let val = Var::new(1);
    let sum = Var::new(0);
    let times = Var::new(0);

    let mut eager = EagerCompute::new(());

    for _ in 0..100 {
        eager.watch(&val, |val| *sum.mutate() += val);
    }

    for _ in 0..100 {
        eager.watch(&sum, |_| *times.mutate() += 1);
    }

    assert_eq!(sum.get_copy(), 100);
    assert_eq!(times.get_copy(), 100);

    val.set(2);

    assert_eq!(sum.get_copy(), 100);
    assert_eq!(times.get_copy(), 100);

    eager.tick();

    assert_eq!(sum.get_copy(), 300);
    assert_eq!(times.get_copy(), 200);

    val.set(3);

    eager.tick();

    assert_eq!(sum.get_copy(), 600);
    assert_eq!(times.get_copy(), 300);
}

#[cfg(test)]
fn write_dot_file(name: &str) {
    reax::ThreadRuntime::write_graphviz(
        File::create(env!("CARGO_MANIFEST_DIR").to_owned() + "/target/" + name + ".dot").unwrap(),
    )
    .unwrap();
}

#[test]
fn nested_reax() {
    let x = Var::new(0).with_label("x");
    let comp_a = computed! { x.get_copy() - 1 }.with_label("comp_a").boxed();
    let comp_b = computed! { x.get_copy() + 1 }.with_label("comp_b").boxed();
    let multi = Var::new(&comp_a).with_label("multi");
    let prod = computed! {
        multi.get().get_copy() * x.get_copy()
    }
    .with_label("prod");

    assert_eq!(prod.get_copy(), 0);
    x.set(1);
    assert_eq!(prod.get_copy(), 0);
    write_dot_file("nested_a");
    multi.set(&comp_b);
    assert_eq!(prod.get_copy(), 2);
    write_dot_file("nested_b");
    x.set(2);
    multi.set(&comp_a);
    assert_eq!(prod.get_copy(), 2);
    multi.set(&comp_b);
    x.set(3);
    assert_eq!(prod.get_copy(), 12);
}

#[test]
fn nested_computed() {
    let x = Var::new(0).with_label("x");
    let switch = Var::new(true).with_label("switch");
    let multi = computed! { match switch.get_copy() {
        true => Rc::new(computed! {
            x.get_copy() + 1
        }.with_label("true").boxed()),
        false => Rc::new(computed! {
            x.get_copy() - 1
        }.with_label("false").boxed()),
    } }
    .with_label("multi");

    let mut eager = EagerCompute::new(());
    eager.install(
        computed! {
            multi.get().get_copy();
        }
        .with_label("value"),
    );

    assert_eq!(multi.node().num_dependencies(), 1);
    assert_eq!(x.node().num_dependents(), 1);

    let a = multi.get().clone();

    switch.set(false);
    assert!(multi.node().is_dirty());
    assert_eq!(x.node().num_dependents(), 1);
    eager.tick();
    assert!(!multi.node().is_dirty());
    assert_eq!(x.node().num_dependents(), 2);
    let b = multi.get().clone();

    switch.set(true);
    assert!(multi.node().is_dirty());
    assert_eq!(x.node().num_dependents(), 2);
    eager.tick();
    assert!(!multi.node().is_dirty());
    assert_eq!(x.node().num_dependents(), 3);
    let c = multi.get().clone();

    assert_eq!(a.get_copy(), c.get_copy());
    assert_ne!(a.get_copy(), b.get_copy());
    assert_ne!(a.node(), c.node());

    x.set(1);
    assert!(!multi.node().is_dirty());
    eager.tick();

    assert!(a.node().is_dirty());
    assert!(b.node().is_dirty());
    assert!(!c.node().is_dirty());

    write_dot_file("nested_computed");
}

#[test]
fn computed_dirties_once() {
    use reax::handler::*;

    let x = Var::new(1);
    let y = (&x).map(|_| ());
    let z = (&x).map(|_| ());

    let mut dirty = DirtiedList::new();
    dirty.attach(y.node());
    dirty.attach(z.node());

    x.set(0); // Ignored since y & z are lazy.
    y.check(&mut ());
    z.check(&mut ());

    for i in 1..=100 {
        x.set(i);
        z.get();
    }

    y.get();

    for i in 1..=100 {
        x.set(i);
        z.get();
    }

    let mut ids_buf = [0; 1000];
    let ids = dirty.poll_dirtied_ids(&mut ids_buf);
    assert_eq!(ids.iter().filter(|&&id| id == y.node().id).count(), 2);
    assert_eq!(ids.iter().filter(|&&id| id == z.node().id).count(), 200);
}

#[test]
fn isolated_computed_runs_once() {
    let times = std::cell::Cell::new(0);
    let x = computed! { times.set(times.get() + 1); };
    assert_eq!(times.get(), 0);
    x.get();
    assert_eq!(times.get(), 1);
    x.get();
    assert_eq!(times.get(), 1);
    x.get();
}

#[test]
fn loops_dont_screw_things_up() {
    let a = Node::next();
    let b = Node::next();
    let c = Node::next();

    a.add_dependency(&b);
    b.add_dependency(&c);
    c.add_dependency(&a);
    c.add_dependency(&b);

    a.on_write(false);

    assert!(!a.is_dirty());
    assert!(b.is_dirty());
    assert!(c.is_dirty());
}

#[test]
fn node_drop_cleans_up() {
    let a = Node::next();
    let b = Node::next();
    let c = Node::next();

    b.add_dependency(&a);
    c.add_dependency(&b);
    assert_eq!(a.num_dependents(), 1);
    assert_eq!(c.num_dependencies(), 1);

    drop(b);

    assert_eq!(a.num_dependents(), 0);
    assert_eq!(c.num_dependencies(), 0);
}

#[test]
fn node_computation_cleans_up() {
    let a = Node::next();
    let b = Node::next();
    let c = Node::next();

    b.add_dependency(&a);
    c.add_dependency(&b);
    assert_eq!(a.num_dependents(), 1);
    assert_eq!(b.num_dependencies(), 1);
    assert_eq!(c.num_dependencies(), 1);

    b.computation(|_| ());

    assert_eq!(a.num_dependents(), 0);
    assert_eq!(b.num_dependencies(), 0);
    assert_eq!(c.num_dependencies(), 1);
}

#[test]
fn tick_timeout() {
    use std::time::{Duration, Instant};
    let timeout = Duration::from_millis(100);

    let a = Var::new(true);
    let b = Var::new(true);
    let mut eager = EagerCompute::new(());
    eager.watch(&a, |a| b.set(!a));

    let start = Instant::now();
    assert!(eager.tick_timeout(start + timeout));
    assert!(start.elapsed() < timeout);

    eager.watch(&b, |b| a.set(*b));

    let start = Instant::now();
    assert!(!eager.tick_timeout(start + timeout));
    assert!(start.elapsed() > timeout);
}

#[test]
fn watchers_set_reactive_areas() {
    let a = Var::new(0);
    let b = Var::new(1);
    let mut eager = EagerCompute::new(());
    eager.watch(&a, |_| {
        b.get();
    });
    assert_eq!(b.node().num_dependents(), 0);
}

#[test]
fn get_non_reactive() {
    let a = Var::new(0);
    let b = computed! { a.get(); };
    let c = computed! {
        a.get_non_reactive();
        b.get_non_reactive();
    };
    c.check(&mut ());

    assert!(b.node().depends_on(a.node()));
    assert!(!c.node().depends_on(a.node()));
    assert!(!c.node().depends_on(b.node()));
}

#[test]
fn replace_current_runtime() {
    let a = Node::next();
    let b = Node::next();
    b.add_dependency(&a);
    a.on_write(false);
    assert!(b.depends_on(&a));
    assert!(!a.is_dirty());
    reax::ThreadRuntime::default().replace_current();
    assert!(!a.depends_on(&b));
    assert!(a.is_dirty());
}

#[test]
fn self_dependent_node() {
    let a = Node::next();
    a.add_dependency(&a);
    assert_eq!(a.num_dependencies(), 0);
}

#[test]
fn graphviz_looks_valid() {
    use std::io::Cursor;

    let a = Node::next();
    a.set_label("node_a");
    let b = Node::next();
    b.set_label("node_b");
    b.add_dependency(&a);
    let c = Node::next();
    c.set_label("node_c");
    c.add_dependency(&b);

    let mut buf = Vec::new();
    reax::ThreadRuntime::write_graphviz(Cursor::new(&mut buf)).unwrap();
    let dot = std::str::from_utf8(&buf).unwrap();
    assert!(dot.starts_with("digraph"));
    for label in &["node_a", "node_b", "node_c"] {
        assert!(dot.contains(label));
    }
    assert_eq!(dot.matches("->").count(), 2);
}
