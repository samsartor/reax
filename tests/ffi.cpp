#include "stdio.h"
#include "assert.h"
#include "../include/reax.hpp"

int main() {
  int times_computed = 0;

  reax::Var<int> test(0);
  auto plus_one = reax::computed([&]() {
    times_computed++;
    return test + 1;
  });

  assert(times_computed == 0);
  assert(reax_node_num_dependencies(plus_one.node) == 0);

  int polls_needed = 0;
  int watchers_called = 0;

  reax::EagerCompute compute { [&]() { polls_needed++; } };
  compute.watch([&]() {
    test.get();
    plus_one.get();
    watchers_called++;
  });
  assert(reax_node_num_dependents(test.node) == 2);
  assert(polls_needed == 0);
  assert(watchers_called == 1);

  assert(plus_one == 1);
  assert(times_computed == 1);
  assert(reax_node_num_dependencies(plus_one.node) == 1);

  test = 1;
  assert(times_computed == 1);
  assert(plus_one == 2);
  assert(times_computed == 2);
  assert(polls_needed == 1);

  assert(compute.poll() == 1);
  assert(compute.poll() == 0);
  assert(watchers_called == 2);

  test = 2;
  assert(polls_needed == 2);

  return 0;
}
